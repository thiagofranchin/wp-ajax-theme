<?php

function listarPosts(){    
    //echo "Listar Posts";

    $page = $_GET['page'];
    $slug = $_GET['slug'];
    $search = $_GET['search'];

    $args = [
        'post_type' => 'post',
        'posts_per_page' => 2, // Post por páginas
        'paged' => ($page)? $page : 1,  // Page existe? senão passa 1
        'category_name' => $slug,
        's' => $search
    ];

    $posts = new WP_Query($args);
    $totalPages = $posts->max_num_pages;
    ?>   

    <?php 
        // Na variável tem posts? have-posts retorna true or false
        if($posts->have_posts()):
    ?>    
        <div class="itens">
            <?php 
                // Enquanto tiver posts, pegue cada post do loop
                while($posts->have_posts()) : $posts->the_post();
                // get_post_meta pega parametros da tabela wp_postmeta do DB
                //                        id Post     meta_key  true = valor do campo meta_value / false = array
                $likes = get_post_meta( get_the_ID(), 'likes', true);
            ?>
            <!-- item -->
            <div class="item" data-id="<?php the_ID(); ?>">
                <div class="card">
                    <div class="card-body">
                        <h4><?php the_title(); ?></h4>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                    <div class="card-footer d-flex">
                        <div class="date mr-auto">
                            <?php the_time('j \d\e F \d\e Y') ?> 
                        </div>
                        <div class="badge badge-secondary"> 
                            <?php the_category(', ') ?>
                        </div>
                        <button type="button" class="btn btn-sm btn-primary btn-detalhes mx-3">Leia mais</button>
                        <button type="button" class="btn btn-sm btn-info btn-curtir" data-tipo="like">
                            <span class="text">Curtir</span> 
                            <span class="badge badge-light"><?php echo ($likes)? $likes : 0; ?></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- fim item -->
        <?php endwhile; ?>	
        </div>

        <!-- paginacao -->
        <?php if($totalPages > 0): ?> 
        <section class="paginacao">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php for( $i = 1; $i <= $totalPages; $i++) : ?>
                    <li class="page-item <?php echo ($i == $page)? 'active' : ''; ?>">
                        <span class="page-link"><?php echo $i; ?></a>
                    </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </section>
        <?php endif ?>
        <!-- fim paginacao -->

        <?php else : ?>
            <div class='alert alert-danger text-center'>Nenhum conteúdo encontrado</div>
        <?php endif ?>
        <!-- <pre class='text-white'>
            <?php print_r($posts); ?>
        </pre> -->
    <?php
    exit; // 'exit' para o retorno não ser zero qdo exibir o resultado
}
// Chama o Hook wp do ajax _nomeDaFuncao + function
add_action( 'wp_ajax_listarPosts', 'listarPosts');
add_action( 'wp_ajax_nopriv', 'listarPosts'); // 'nopriv' para usuários não logados