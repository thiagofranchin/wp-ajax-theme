<?php

function curtirPostToggle(){

    $id = $_GET['id'];
    $tipo = $_GET['tipo'];

    // Pega qtd likes do DB
    $likes = get_post_meta( $id, 'likes', true );

    if($tipo == 'like'){        
        // id Post / valor da coluna meta_key / valor meta_value do db +1 / valor atual do meta_value  
        update_post_meta( $id, 'likes', $likes + 1, $likes );
    }else{        
        update_post_meta( $id, 'likes', $likes - 1, $likes );
    }

    // Pega a qtd de likes atualizada
    $likes = get_post_meta( $id, 'likes', true );

    echo $likes;

    exit; // 'exit' para o retorno não ser zero qdo exibir o resultado
}
// Chama o Hook wp do ajax _nomeDaFuncao + function
add_action( 'wp_ajax_curtirPostToggle', 'curtirPostToggle');
add_action( 'wp_ajax_nopriv_curtirPostToggle', 'curtirPostToggle');  // 'nopriv' para usuários não logados