<?php

function detalhesPost(){
    
    get_template_part('_parts/detalhes');
    exit; // 'exit' para o retorno não ser zero qdo exibir o resultado
}
// Chama o Hook wp do ajax _nomeDaFuncao + function
add_action( 'wp_ajax_detalhesPost', 'detalhesPost');
add_action( 'wp_ajax_nopriv', 'detalhesPost'); // 'nopriv' para usuários não logados