<?php
    // Trás todos os termos de uma taxonomia
    $categorias = get_terms('category');
?>

<!-- categorias -->
<ul class="list-group">
    <li class="list-group-item active" data-slug="">Todos</li>
    
    <?php // Se a categoria não estiver vazia e não tiver erros
    if(!empty($categorias) && !is_wp_error($categorias)): ?>
        <?php foreach($categorias as $categoria): ?>
            <li class="list-group-item" data-slug="<?php echo $categoria->slug; ?>"><?php echo $categoria->name; ?></li>    
        <?php endforeach ?>
    <?php endif ?>

</ul>
<!-- fim categorias -->

<!-- <pre class="text-white">
    <?php // print_r($categorias); ?>
</pre> -->