jQuery(function ($) {
    /**************************
     * Listar Posts
     **************************/

    var page = 1;
    var slug = $('.list-group-item.active').data('slug');
    var search = $('#campo-busca').val();

    var rlp = null;

    var listarPostAjax = function (page, slug, search) {

        rlp = $.ajax({
            url: wp.ajaxurl,
            type: 'GET',
            data: {
                action: 'listarPosts',
                page: page,
                slug: slug,
                search: search
            },
            beforeSend: function () {
                $('#loading .progress').removeClass('d-none');
                //console.log('Carregando Posts...');
                if(rlp != null){
                    rlp.abort();
                    rlp = null;
                }
            }
        }).done(function (resposta) {
            //console.log(resposta);
            $('#loading .progress').addClass('d-none');
            $('#lista-posts').html(resposta);
            visitanteLikes();
        }).fail(function () {
            console.log('Ops, quebro listar!!!');
        });

    }

    listarPostAjax(page);

    // Ação do botão da categoria.
    $('.list-group-item').on('click', function(){
        slug = $(this).data('slug');
        search = $('#campo-busca').val();
        listarPostAjax(1, slug, search); // Passa 1 para sempre listar os posts da página 1

        $('.list-group-item').removeClass('active');
        $(this).addClass('active');
    });


    // Click na paginação.
    $('body').on('click', '.page-item', function(){
        // Pega o nº do botão e passa para a variável que passa para a função de listar com o nº da página a ser exibida.
        page = $(this).find('span').text();
        
        listarPostAjax(page, slug, search);
        $('.page-item').removeClass('active');
        $(this).addClass('active');
    });

    // Ação limpar busca.
    $('body').on('click', '#btn-limpar', function(){
        listarPostAjax(1);
        $(this).addClass('d-none');
        $('#campo-busca').val('');
        
        $('.list-group-item').removeClass('active');
        $('.list-group-item[data-slug=""]').addClass('active');
        search = ''; // Limpa a var search para qdo clicar no menu de categorias não trazer o que estava escrito no input da busca
    })

    // Ação ao digitar uma palavra na busca
    $('body').on('keyup', '#campo-busca', function(){
        search = $('#campo-busca').val();

        if(search.length >= 3){
            listarPostAjax(1, slug, search);
        }else{
            listarPostAjax(1, slug);
        }

        if(search.length == 0){
            $('#btn-limpar').addClass('d-none');
        }else{
            $('#btn-limpar').removeClass('d-none');
        }
    })



    /**************************
     * Detalhe Post
     **************************/

    var detalhesPostAjax = function (id) {
        $.ajax({
            url: wp.ajaxurl,
            type: 'GET',
            data: {
                action: 'detalhesPost',                
                id: id
            },
            beforeSend: function () {
                $('#loading .progress').removeClass('d-none');
                //console.log('Carregando detalhes do posts...');
            }
        }).done(function(resposta) {            
            $('#loading .progress').addClass('d-none');
            // Adiciona conteúdo de detalhes (Network resposta)
            $('#detalhes-post').html(resposta);
            // Abre o modal "Leia mais"
            $('#detalhes-post').modal('show');
            //console.log(resposta);
        }).fail(function () {
            console.log('Ops, quebro detalhes!!!');
        });
    }

    //detalhesPostAjax();

    // Ação do botão "Leia mais"

    $('body').on('click', '.btn-detalhes', function(){        
        let id = $(this).closest('.item').data('id');        
        detalhesPostAjax(id);
        
    });

    /**************************
     * Curtir e descurtir Posts
     **************************/

    var curtirPostToggleAjax = function(id, tipo) {
        $.ajax({
            url: wp.ajaxurl,
            type: 'GET',
            data: {
                action: 'curtirPostToggle',
                id: id,
                tipo: tipo
            },
            beforeSend: function () {
                $('#loading .progress').removeClass('d-none');
                console.log('Curtindo post');
            }
        }).done(function (resposta) {
            $('#loading .progress').addClass('d-none');
            if(tipo == 'like') {
                $('[data-id=' + id + '] .btn-curtir').data('tipo', 'deslike');
                $('[data-id=' + id + '] .btn-curtir').removeClass('btn-info').addClass('btn-success');
            }else{
                $('[data-id=' + id + '] .btn-curtir').data('tipo', 'like');
                $('[data-id=' + id + '] .btn-curtir').removeClass('btn-success').addClass('btn-info');
            }
            $('[data-id=' + id + '] .btn-curtir .badge').html(resposta);
            //console.log(resposta);
        }).fail(function () {
            console.log('Ops, quebro curtir!!!');
        });
    }
    //curtirPostToggleAjax();

    // Ação do botão curtir
    $('body').on('click', '.btn-curtir', function(){
        let id = $(this).closest('.item').data('id'); 
        let tipo = $(this).data('tipo');
        curtirPostToggleAjax(id, tipo);

        let salvos = localStorage.getItem('likes');

        if(!salvos){
            //                 Nome(key) | Valor(value)
            localStorage.setItem('likes', id.toString()); // Coloca .toString() pois o local Storage armazena o os id em string, e para comparar depois já passamos o id convertido em string
        }else{
            salvos = salvos.split(',');
            let item = $.inArray(id.toString(), salvos); // Se existir retorna o indice, se não existir retorna "-1"
            if(item == -1){
                salvos.push(id.toString());
            }else{
                salvos.splice(item, 1); //Qtos item quero remover a partir dele mesmo, no caso 1 pois irei remover apenas ele mesmo
            }
            localStorage.setItem('likes', salvos);
        }
    });

    var visitanteLikes = function(){
        let salvos = localStorage.getItem('likes');
        if(salvos){
            salvos = salvos.split(',');
            salvos.forEach( function(id){
                $('[data-id=' + id + '] .btn-curtir').removeClass('btn-info').addClass('btn-success');
                $('[data-id=' + id + '] .btn-curtir').attr('data-tipo', 'deslike');
            });
        }
    }
})