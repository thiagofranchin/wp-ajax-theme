<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'ajaxwp');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3Z*3^tEyWL,t.~ [S^-MJIfLfsso4-LkoU:KFC!`S-.5uCj]?]6FW/XCqk@KD(7.');
define('SECURE_AUTH_KEY',  'Z`#Tq`E]&6SWJ9P#gk*(7cT:XmW--miC[j2MF^UhD0)w?Ft<rG7zz){S/FonYFD<');
define('LOGGED_IN_KEY',    'b)+ho0;Ev6[nQaLAXd0N6-uxf8yM:%=Q^uHM=U{8m286U :?f%^Q,6%P+@xUTl1Z');
define('NONCE_KEY',        'PJ^#TFfCUWG*JY[;;i9ymxxnVd^D+t3XcOoTnDIMyE^$-())2EVx:$#gJAqBOXLC');
define('AUTH_SALT',        'R7k_:!PnTvb9-[4HC8wUV!oxa[!nLO7!e>/x27rTlEXT!3HdK6&n0`?`A1:L?a#I');
define('SECURE_AUTH_SALT', 'oH/x?l{pBTOF:4$i>=k<$7DVspKS*}<dvZZKu,PC3i=|YG;uC8xw.@:X!Tq^[[|~');
define('LOGGED_IN_SALT',   'C#UF(#+;yA>>&2):RM(rS1W^pD_GwbMh|Gca4a%gOCb9R*PpB</&rSk#m1C|G9)B');
define('NONCE_SALT',       'Kzx]BFb8)kg (AY1::(}ko4>BHYuy[tq KkQ2sH;5SvNn_NjxOz^3IA`29^6{D<o');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ajwpfk_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
